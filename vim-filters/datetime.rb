#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require "Date"
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: pipe lines (usually from vim) to this file
　　　ＶＩＭ　ＵＳＡＧＥ:: :'<,'> !datetime.rb
　ＤＥＳＣＲＩＰＴＩＯＮ:: returns each lines as iso8610 dates.
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 4
=end ——————————————————————————————————————————————————————————————————————————

STDIN.each { |vim| puts DateTime.parse vim }
