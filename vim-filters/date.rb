#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require "Date"
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: pipe lines (usually from vim) to this file
　　　ＶＩＭ　ＵＳＡＧＥ:: `:'<,'> !date.rb`
　ＤＥＳＣＲＩＰＴＩＯＮ:: returns each lines as iso8610 dates.
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 3
=end ——————————————————————————————————————————————————————————————————————————

STDIN.each { |vim| puts Date.parse vim }
