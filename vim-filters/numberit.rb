#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: pipe lines (files) (usually from vim) to this file
　ＤＥＳＣＲＩＰＴＩＯＮ:: returns a list of numbered files with the same extention as the inputed files.（generaly used for CBZ）
ＤＥＰＥＮＤＥＮＣＩＥＳ:: 
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 4
=end ——————————————————————————————————————————————————————————————————————————
def numberit
    c = 0
    liste = []
    STDIN.each { |ii| liste << ii }
    l = liste.length.to_s.length

    liste.each {|f| 
        c += 1
        if %r{[Jj][Pp][Ee][Gg]}.match(f.split(".").last) then 
            puts %Q{#{format("%0#{l}d", c)}.jpg}
        else 
            puts %Q{#{format("%0#{l}d", c)}.#{f.split('.').last}}
        end
    }
end

numberit
