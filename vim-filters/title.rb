#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$termLength = 173
$font = "sblood"
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: pipe text to it.
　ＤＥＳＣＲＩＰＴＩＯＮ:: Asciiart-ize the text with figlet and center it to the defined Termlength
ＤＥＰＥＮＤＥＮＣＩＥＳ:: figlet
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 5
=end ——————————————————————————————————————————————————————————————————————————
def title
    if $*.length != 0 then
        χtemp = `figlet -w #{$termLength} -f #{$font} #{$*.join " "}`
        return χtemp.split("\n").each do |ii| puts ii.center $termLength end
    else
        ARGF.read.split("\n").each { |vim|
            χtemp = `figlet -w #{$termLength} -f #{$font} #{vim}`
            χtemp.split("\n").each do |vim| puts vim.to_s.center $termLength end
        }
    end
end

title
