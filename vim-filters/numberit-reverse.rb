#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: pipe lines (files) (usually from vim) to this file
　ＤＥＳＣＲＩＰＴＩＯＮ:: returns a list of numbered files with the same extention as the inputed files.（generaly used for CBZ） 
ＤＥＰＥＮＤＥＮＣＩＥＳ:: 
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 3
=end ——————————————————————————————————————————————————————————————————————————
def numberit_reverse
    liste = []
    STDIN.each { |ii| liste << ii }
    c = liste.length + 1
    l = liste.length.to_s.length

    liste.each {|f| 
        c -= 1
        if %r{[Jj][Pp][Ee][Gg]}.match(f.split(".").last) then 
            puts %Q{#{format("%0#{l}d", c)}.jpg}
        else 
            puts %Q{#{format("%0#{l}d", c)}.#{f.split('.').last}}
        end
    }
end

numberit_reverse
