#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require "rqrcode"
$moduleSize = 6
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: pipe lines (usually from vim) to this file
　　　ＶＩＭ　ＵＳＡＧＥ:: :'<,'> !ad-qr.rb
　ＤＥＳＣＲＩＰＴＩＯＮ:: creates a matrix codebar with the inputed text.
ＤＥＰＥＮＤＥＮＣＩＥＳ:: rqrcode
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 2
=end ——————————————————————————————————————————————————————————————————————————

def main
    res = []
    STDIN.each { |vim|
        res << vim
    }
    print("+++")
    RQRCode::QRCode.new( res.join("\n") ).as_svg( color: '000', module_size: $moduleSize, offset: $moduleSize*4, ).split("\n").each do |ii|
        puts "    #{ii}"
    end
    puts "+++"
end

main
