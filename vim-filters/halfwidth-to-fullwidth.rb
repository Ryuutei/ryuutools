#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin —————————————————————————————————————————————————————————————————————————
　　　ＶＩＭ　ＵＳＡＧＥ:: '<,'> ! hw2fw.rb
　ＤＥＳＣＲＩＰＴＩＯＮ:: Translate from half width to full width characters.
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 3
=end ———————————————————————————————————————————————————————————————————————————

##
# Half width convertion table
hwfw =[
    %Q{0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ,.:;!?"'`^~¯_&@#%+*=<>()[]{}⸨⸩|¦/\\\\¬$£¢¥-}, 
    %Q{０１２３４５６７８９ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ　，．：；！？＂＇｀＾～￣＿＆＠＃％＋＊＝＜＞（）［］｛｝｟｠｜￤／＼￢＄￡￠￥－} 
]

ARGF.read.split("\n").each { |vim| puts "#{vim.to_s.tr(hwfw[0], hwfw[1] )}" }
