#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require "chinese_pinyin"
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: pipe lines (usually from vim) to this file
　　　ＶＩＭ　ＵＳＡＧＥ:: :'<,'> !asciidoc-pinyin.rb
　ＤＥＳＣＲＩＰＴＩＯＮ:: returns each lines in a ruby container with its pinyin. （asciidoc）
ＤＥＰＥＮＤＥＮＣＩＥＳ:: chinese_pinyin（https://github.com/flyerhzm/chinese_pinyin）
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 3
=end ——————————————————————————————————————————————————————————————————————————

STDIN.each { |ii| 
    puts "+++<ruby>#{ii.chomp}<rt>#{Pinyin.t(ii.chomp, tonemarks: true)}</rt></ruby>+++"
}
