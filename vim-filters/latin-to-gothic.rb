#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ————————————————————————————————————————————————————————————————————————
　　　ＶＩＭ　ＵＳＡＧＥ:: :'<,'> !latin-to-gothic.rb
　ＤＥＳＣＲＩＰＴＩＯＮ:: Replace English alphabets to Unicode gothic characters.
ＤＥＰＥＮＤＥＮＣＩＥＳ:: 
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕based on Xah Lee's original idea http://ergoemacs.org/misc/thou_shalt_use_emacs_lisp.html
　　　　ＲＥＶＩＳＩＯＮ:: 2
=end ——————————————————————————————————————————————————————————————————————————
latinToGothic = ["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz","𝔄𝔅ℭ𝔇𝔈𝔉𝔊ℌℑ𝔍𝔎𝔏𝔐𝔑𝔒𝔓𝔔ℜ𝔖𝔗𝔘𝔙𝔚𝔛𝔜ℨ𝔞𝔟𝔠𝔡𝔢𝔣𝔤𝔥𝔦𝔧𝔨𝔩𝔪𝔫𝔬𝔭𝔮𝔯𝔰𝔱𝔲𝔳𝔴𝔵𝔶𝔷"]
STDIN.each { |l| puts l.tr(latinToGothic[0], latinToGothic[1]) }
