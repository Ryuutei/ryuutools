#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ————————————————————————————————————————————————————————————————————————
      ____ _                           _     _
     / ___| | __ _ ___ ___    __ _  __| | __| | ___  _ __  ___
    | |   | |/ _` / __/ __|  / _` |/ _` |/ _` |/ _ \| '_ \/ __|
    | |___| | (_| \__ \__ \ | (_| | (_| | (_| | (_) | | | \__ \
     \____|_|\__,_|___/___/  \__,_|\__,_|\__,_|\___/|_| |_|___/

　　　　　　　ＵＳＡＧＥ:: load "classaddons.rb"
　ＤＥＳＣＲＩＰＴＩＯＮ:: adds useful stuff to some classes
ＤＥＰＥＮＤＥＮＣＩＥＳ::
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 1
=end ——————————————————————————————————————————————————————————————————————————

class Date

    ##
    # returns the date in chinese
    #
    # return:: `str`
    def chinese
        χmonth = %W{␀ ㋀ ㋁ ㋂ ㋃ ㋄ ㋅ ㋆ ㋇ ㋈ ㋉ ㋊ ㋋}
        χday = %W{␀ ㏠ ㏡ ㏢ ㏣ ㏤ ㏥ ㏦ ㏧ ㏨ ㏩ ㏪ ㏫ ㏬ ㏭ ㏮ ㏯ ㏰ ㏱ ㏲ ㏳ ㏴ ㏵ ㏶ ㏷ ㏸ ㏹ ㏺ ㏻ ㏼ ㏽ ㏾}
        return "#{self.year}年#{χmonth[self.month]}#{χday[self.day]}"
    end

end

class String

    ##
    # good enough for Ruby 2.2
    def title
        return self.split.map(&:capitalize).join(" ")
    end

    ##
    # Should works correctly in ruby 2.4
    def title24
        return self.split(/(\W)/).map(&:capitalize).join
    end

end

class Integer

    ##
    # returns the UNICODE roman numeral.
    # infos at http://www.novaroma.org/via_romana/numbers.html
    #
    # return:: `str`
    def roman
        res = []
        i = self
        romanNumerals = [ [100000, "ↈ"],
                [ 90000, "ↂↈ"],
                [ 50000, "ↇ"],
                [ 40000, "ↂↇ"],
                [ 10000, "ↂ"],
                [  9000, "Ⅿↂ"],
                [  5000, "ↁ"],
                [  4000, "Ⅿↁ"],
                [  1000, "Ⅿ"],
                [   900, "ⅭⅯ"],
                [   500, "Ⅾ"],
                [   400, "ⅭⅮ"],
                [   100, "Ⅽ"],
                [    90, "ⅩⅭ"],
                [    50, "Ⅼ"],
                [    40, "ⅩⅬ"],
                [    10, "Ⅹ"],
                [     9, "Ⅸ"],
                [     8, "Ⅷ"],
                [     7, "Ⅶ"],
                [     6, "Ⅵ"],
                [     5, "Ⅴ"],
                [     4, "Ⅳ"],
                [     3, "Ⅲ"],
                [     2, "Ⅱ"],
                [     1, "Ⅰ"],
        ]

        romanNumerals.each { |a,ρ|
            while i - a >= 0 do
                i -= a
                res << ρ
            end
        }
        return res.join
    end

end

