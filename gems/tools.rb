#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ————————————————————————————————————————————————————————————————————————
　ＤＥＳＣＲＩＰＴＩＯＮ:: Tools for ruby.
ＤＥＰＥＮＤＥＮＣＩＥＳ::
　　　　ＲＥＶＩＳＩＯＮ:: 5
=end ——————————————————————————————————————————————————————————————————————————
$τ = 2*Math::PI
$φ = phi = (1+ 5**0.5)/2
# —————————————————————————————————————————————————————————————————————————————
#   __                  _   _
#  / _|_   _ _ __   ___| |_(_) ___  _ __  ___
# | |_| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
# |  _| |_| | | | | (__| |_| | (_) | | | \__ \
# |_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
#
# —————————————————————————————————————————————————————————————————————————————

##
# throws a die
#
# δ:: `int`, length of the die.
#
# return:: `int`
def d(δ) rand(1..δ) end

##
# returns the area of the circle with the radius `r'
#
# r:: `int` or `float`, Radius
#
# return:: `float`
def circle_area(r)
    (1.0/2.0) * $τ * r.to_f  ** 2.0
end

##
# returns the distance in 3D.
#
# pos1:: `array` coordinates `x`, `y`, `z`
#
# pos2:: `array` coordinates `x`, `y`, `z`
#
# return:: `float`
def distance3D(pos1, pos2)
    x = ( pos1[0] - pos2[0] )**2
    y = ( pos1[1] - pos2[1] )**2
    z = ( pos1[2] - pos2[2] )**2
    (x + y + z)**0.5
end

##
# returns the distance.
#
# pos1:: `array` coordinates `x`, `y`
#
# pos2:: `array` coordinates `x`, `y`
#
# return:: `float`
def distance(pos1, pos2)
    x = ( pos1[0] - pos2[0] )**2
    y = ( pos1[1] - pos2[1] )**2
    (x + y)**0.5
end

##
# returns the normalization of the vector.
# normalize([3,4,5,6])
#  => [0.3234983196103152, 0.43133109281375365, 0.539163866017192, 0.6469966392206304]
#
# http://xahlee.info/UnixResource_dir/writ/Mathematica_expressiveness.html
# Ruby 1.8.7. By w_a_x…@yahoo.com
#
# can also be done with:
# require "matrix"
# Vector.element( vec ).normalize
def normalize vec
    s = Math.sqrt(vec.map{|x|x*x}.inject{|x,y|x+y})
    vec.map{|x| x/s}
end

