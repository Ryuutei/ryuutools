#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ：extip.rb
　ＤＥＳＣＲＩＰＴＩＯＮ：retrieve the external ip form a neufbox
ＤＥＰＥＮＤＥＮＣＩＥＳ：wget
　　　　　　ＡＵＴＨＯＲ：Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ：2
=end ——————————————————————————————————————————————————————————————————————————

puts %r{([0-9]{1,3}\.){3}[0-9]{1,3}}.match(`wget -qO - http://192.168.1.1`.chomp)
