#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require "digest"
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: rensha.rb <files>
　ＤＥＳＣＲＩＰＴＩＯＮ:: rename files with their own SHA1 checksum
ＤＥＰＥＮＤＥＮＣＩＥＳ:: ruby 2
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 2
=end ——————————————————————————————————————————————————————————————————————————
def rensha
    $*.each { |f|
        b = File.dirname(f)
        h = Digest::SHA1.file(f).hexdigest
        x = File.extname(f)
        if %r{[Jj][Pp][Ee][Gg]}.match x then x = ".jpg" end
        File.rename(f, File.expand_path(h+x, b))
        puts %Q{#{h+x.downcase} #{f}}
    }
end

rensha
