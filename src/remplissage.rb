#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: remplissage.rb <columns> <rows> <image to multiply> <output>
　ＤＥＳＣＲＩＰＴＩＯＮ:: 
ＤＥＰＥＮＤＥＮＣＩＥＳ:: 
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 1
=end ——————————————————————————————————————————————————————————————————————————
$imagemagick = %x{which convert}.chomp

def main( α, ν, ε)
    res = []

    α[1].times { res << "\\( #{Array.new(α[0], ν).join(" ")} +append \\)" }
    res << "-append"
    res << ε

    %x{#{$imagemagick} #{res.join(" ")}}
    true
end

if File.split($0).last == "remplissage.rb" then
    begin
        main( [ $*[0].to_i, $*[1].to_i], $*[2], $*[3] )
    rescue
        puts
        puts "Usage::"
        puts "    remplissage.rb <columns> <rows> <image to multiply> <output>"
        puts
    end
end
