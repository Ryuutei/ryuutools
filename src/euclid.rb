#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ========================================================================
　　　　　　　ＵＳＡＧＥ：euclid <int> <int>
　ＤＥＳＣＲＩＰＴＩＯＮ：returns the greatest common divisor 
ＤＥＰＥＮＤＥＮＣＩＥＳ：
　　　　　　ＡＵＴＨＯＲ：Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ：2
=end ==========================================================================
# returns the greatest common divisor 
# α&β::     int
# return::  int
def euclid(α, β)
    return α.divmod(β)[0]
end

def main
    puts "#{$*[0]} % #{$*[1]} = #{$*[0].to_i.divmod($*[1].to_i)[0]}"
end

main
