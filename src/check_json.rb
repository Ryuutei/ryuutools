#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
begin
    require "yajl"
    $jsonmodule = false
rescue
    require 'json'
    $jsonmodule = true
end
$version = "1–10"

=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: check_json.rb <files>
　ＤＥＳＣＲＩＰＴＩＯＮ:: returns if the files have a json compliant structure
ＤＥＰＥＮＤＥＮＣＩＥＳ:: yajl or json
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 9
=end ——————————————————————————————————————————————————————————————————————————
def check_json
    puts "\n\n〔check_json #{$version}〕:: "

    $*.each {|f| 
        open(f, "r") { |lf| 
            if $jsonmodule then 
                begin 
                    if JSON.parse(lf.read) then 
                        puts "    \x1b[32m✓ #{f}\x1b[0m" 
                    end
                rescue  JSON::ParserError
                        puts "    \x1b[31m⚠ #{f} parsing failed.\x1b[0m"
                end
            else
                begin 
                    if Yajl::Parser.parse(lf.read).inspect then 
                        puts "    \x1b[32m✓ #{f}\x1b[0m" 
                    end
                rescue Yajl::ParseError => error
                        puts "    \x1b[31m⚠ #{f} parsing failed.\x1b[0m"
                        puts error
                end
            end
        }
    }

end

check_json
