#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ——————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: check_playlist.rb <cmus playlist>
　ＤＥＳＣＲＩＰＴＩＯＮ:: checks all entries in a cmus playlist and prints all files that are not here.
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 3
TODO:: a playlist generator in ruby with JSON Spiff http://www.xspf.org/jspf/
=end ————————————————————————————————————————————————————————————————————————————
def check_playlist
    e = 0
    t = 0
    $*.each { |plf|
        open(plf, "r") { |f|
            f.read.split("\n").each { |ii|
                t += 1
                if not File.exist? ii then 
                    e += 1
                    puts "⚠#{plf}：#{ii}"
                end
            }
        }
    }
    puts "#{e} / #{t}"
end

check_playlist
