#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require "json"
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: jsonpp.rb <files> ; you can also add this line to your zsh environment.  `alias -s json="jsonp.rb"`
　ＤＥＳＣＲＩＰＴＩＯＮ:: pretty print json files.
ＤＥＰＥＮＤＥＮＣＩＥＳ:: ruby2, json
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 4
=end ——————————————————————————————————————————————————————————————————————————
def jsonpp
    $*.each { |ii| puts JSON.pretty_generate JSON.parse open(ii).read }
end

jsonpp
