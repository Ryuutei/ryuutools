#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require "json"
require "edn_turbo"
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: json2edn.rb <file.json> <file.edn>
　ＤＥＳＣＲＩＰＴＩＯＮ:: crudely converts a json db into edn
ＤＥＰＥＮＤＥＮＣＩＥＳ:: json, turbo_edn
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 2
=end ——————————————————————————————————————————————————————————————————————————
def json_to_edn
    temp = JSON.parse open($*[0] ) { |f| f.read }

    if File.exist?($*[1]) then
        puts "⚠ #{$*[1]} exists."
        return 1
    else
        open($*[1], 'w') { |f| f.puts( temp.to_edn ) }

    end
end

json_to_edn
