#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$ppedn = "/Users/ryuutei/Dropbox/bin/jar/ppedn-0.1.0-SNAPSHOT-standalone.jar"
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: ednpp.rb <files>
　ＤＥＳＣＲＩＰＴＩＯＮ:: uses ppedn to pretty print edn files
ＤＥＰＥＮＤＥＮＣＩＥＳ:: java, clojure, ppedn.clj
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 3
=end ——————————————————————————————————————————————————————————————————————————
def ednpp
    $*.each { |ii|
        puts `cat #{ii} | java -jar #{$ppedn}`
    }
end

ednpp
