#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$magick = "/usr/local/bin/convert"
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: txt2tweet.rb <a txt file>
　ＤＥＳＣＲＩＰＴＩＯＮ:: Creates a picture with a text file that can be read when sent to twitter.
ＤＥＰＥＮＤＥＮＣＩＥＳ:: imagemagick
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝
　　　　ＲＥＶＩＳＩＯＮ:: 3
=end ——————————————————————————————————————————————————————————————————————————
=begin ————————————————————————————————————————————————————————————————————————
https://inkscape.org/en/doc/inkscape-man.html
pour quelque raisons inkscape Mac OSⅩ ne fonctionne pas en no-gui

$inkscape = ["/Applications/Inkscape.app/Contents/Resources/bin/inkscape-bin",  "--without-gui", ]
%Q{#{$inkscape[0]} #{$inkscape[1]} --file=#{} --export-png=#{}}
=end ——————————————————————————————————————————————————————————————————————————
χinput  = $*[0]
χoutput = $*[0].split(".")[0...-1].push("png").join(".")
#χinput  = "/Users/ryuutei/test/aoeui.htns.txt"
#χoutput = "/Users/ryuutei/test/aoeui.htns.txt".split(".")[0...-1].push("png").join(".")


##
#opens a txt file and returns the whole file cleaned up for shell in one line of string.
def opentxt(α)
    chars = {
        %r{\\}=> '／',
        %r{/} => '／',

        %r{\? ?} => '？',
        %r{; ?} => '；',
        %r{, ?} => '，',
        %r{\. ?} => '．',
        %r{! ?} => '！', #1
        %r{@} => '＠',
        %r{#} => '＃',
        %r{\$} => '＄',
        %r{%} => '％',
        %r{\^} => '＾',
        %r{ ?& ?} => '＆',
        %r{\*} => '＊',
        %r{ ?\(} => '（', #9
        %r{\) ?} => '）', #0
        %r{=} => '＝',
        %r{\+} => '＋',

        %r{ ?\{} => '｛',
        %r{\} ?} => '｝',
        %r{ ?\[} => '［',
        %r{\] ?} => '］',
        %r{> ?} => '〉',
        %r{< ?} => '〈',

        %r{`} =>  '｀',
        %r{~} =>  '～',
        
        "\"" =>  '\\"',
        "'" =>   %Q{’}, 
        "\n" => "\\n", 
       ##%r{-} => 'ー',
        ##%r{_} => '＿',
    }

    res = open(α, "r").read()
    chars.each { |r, u| res.gsub!(r, u) }
    res
end

$caption = [
    "-size 800x", 
    "-background white", "-fill black",
    "-gravity West",
    "-font /Library/Fonts/Hannotate.ttc", "-pointsize 42",
    %Q{caption:"#{opentxt χinput}"},
]

if File.split($0).last == "txt2tweet.rb" then
    `#{$magick} #{$caption.join(" ")} "#{χoutput}"`
end
