#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require "edn"
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: check_edn.rb <files>
　ＤＥＳＣＲＩＰＴＩＯＮ:: returns if the files have a edn compliant structure
ＤＥＰＥＮＤＥＮＣＩＥＳ:: ruby2, edn
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 3
=end ——————————————————————————————————————————————————————————————————————————
def checkEDN
    puts "\n\n〔check_edn〕:: "

    #puts JSON.generate( open($*[0], "r") { |f| JSON.parse(f.read)})
    $*.each {|f| 
        open(f, "r") { |lf| 
            begin
                if a = EDN::Parser.new( open('douchevahkiin.edn')).read then 
                    puts "    ✓ #{f}" 
                end
            rescue
                    puts "    ⚠ #{f} parsing failed."
            end
        }
    }

end

checkEDN
