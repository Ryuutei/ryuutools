#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ：endian.rb <integers>
　ＤＥＳＣＲＩＰＴＩＯＮ：prints each integers to hexadecimal big endian notation
ＤＥＰＥＮＤＥＮＣＩＥＳ：nil
　　　　　　ＡＵＴＨＯＲ：Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ：2
=end ——————————————————————————————————————————————————————————————————————————
#[å].pack("Q") # unsigned 64bits Big endian   # > little endian
#[å].pack("L") # unsigned 32bits
#[å].pack("V") # unsigned 16bit

def endian(å, t=1)
    # t=0   little endian
    # t=1   big endian
    res = []
    if t <= 0 then  [å].pack("Q>").each_byte { |i| res << "#{i.to_s(16).rjust(2, "0")}" }
    else            [å].pack("Q").each_byte  { |i| res << "#{i.to_s(16).rjust(2, "0")}" } end

    res
end

$*.each { |å|
    endian(å.to_i).each { |ii|  print ii + " "}
}
print "\n"
