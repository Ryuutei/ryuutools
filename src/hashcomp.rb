#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: hashcomp <md5, sha1> <hash, file> <hash, file>
　ＤＥＳＣＲＩＰＴＩＯＮ:: Compares a hash  string to a file or a file to a file.
ＤＥＰＥＮＤＥＮＣＩＥＳ:: openssl
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 2
=end ——————————————————————————————————————————————————————————————————————————
def hashcomp
    if $*.length == 2 then
        if File.exists? $*[0] and File.exists? $*[1] then puts compareFiles($*[0], $*[1])
        else hashcomp_helper
        end
    elsif  $*.length == 3 then
        if File.exists? $*[1]       then puts compare($*[0], $*[1], $*[2])
        elsif File.exists? $*[2]    then puts compare($*[0], $*[2], $*[1])
        end
    else hashcomp_helper
    end
end

##
# prints the help for the function `hashcomp`
def hashcomp_helper
    puts "\nhashcomp\n"
    puts "    ＵＳＡＧＥ:: "
    puts "    hashcomp <file> <file>"
    puts "    or"
    puts "    hashcomp <md5, sha1> <hash, file> <hash, file>"
end

##
# compares α & β with the δ hashing method (sha1 or md5)
# δ:: `str`, digest method.
#
# α:: `str`, file
#
# β:: `str`, hash hexdigest.
#
# return:: `bool`
def compare(δ, α, β)
    if   δ == "sha1" then /[0-9a-f]{40}$/.match(`openssl dgst -#{δ} #{α}`.chop).to_s == β
    elsif δ == "md5" then /[0-9a-f]{32}$/.match(`openssl dgst -#{δ} #{α}`.chop).to_s == β
    end
end

##
# compare two files α & β with the sha1 hash method.
#
# α:: `str`, filename
#
# β:: `str`, filename
#
# return:: `bool`
#
def compareFiles(α, β)
    /[0-9a-f]{40}$/.match(`openssl dgst -sha1 "#{α}"`.chop).to_s == /[0-9a-f]{40}$/.match(`openssl dgst -sha1 "#{β}"`.chop).to_s
end

hashcomp
