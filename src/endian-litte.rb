#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ：littleendian.rb <integers>
　ＤＥＳＣＲＩＰＴＩＯＮ：prints each integers to hexadecimal little endian notation
ＤＥＰＥＮＤＥＮＣＩＥＳ：nil
　　　　　　ＡＵＴＨＯＲ：Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ：2
=end ——————————————————————————————————————————————————————————————————————————
def endian(å, t=1)
    # t=0   little endian               t=1   big endian
    res = []
    if t <= 0 then  [å].pack("Q>").each_byte { |i| res << "#{i.to_s(16).rjust(2, "0")}" }
    else            [å].pack("Q").each_byte  { |i| res << "#{i.to_s(16).rjust(2, "0")}" } end

    res
end

$*.each { |å|
    endian(å.to_i, 0).each { |ii|  print ii + " "}
}
print "\n"
