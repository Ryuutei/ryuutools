#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: catp8.rb <files.p8>
　ＤＥＳＣＲＩＰＴＩＯＮ:: prints the interesting（lua）part of the Pico8 files.
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 2
=end ——————————————————————————————————————————————————————————————————————————
def catp8
    $*.each { |ƒ|
        puts "＃Ｆｉｌｅ:: #{ƒ}"
        open(ƒ, "r") { |f|
            f.each_line { |l| 
                if l.chomp == "__gfx__" then break end
                puts l
            }
        }
    }
end

catp8
