#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require "date"
require "json"
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: nowplaying.rb <track|album>
　ＤＥＳＣＲＩＰＴＩＯＮ:: prints the song or album currently playing.
ＤＥＰＥＮＤＥＮＣＩＥＳ:: cmus, csg(non public yet)
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 1
=end ——————————————————————————————————————————————————————————————————————————
$db = JSON.parse( open("/Users/ryuutei/.cmus/csg.ndb").read() )


##
# return a random note
def note(α=true) ; 
    notes = [ '𝄞','♫','♬','♩','♪', '🎶', '🎵', ]
    if α == true then return notes.sample 
    else notes[1..-1].sample 
    end
end 

# Generates a string to send to twitter.
# 
# t = track or album
# 
def gen_output(t='track' )
    tweet = [ "🐉 Now playing：", note() ]

    if t == 'track' then    tweet << smartQuote("『#{$db['title']}』by ")
    else                    tweet << smartQuote("〔#{$db['album']}〕by ")
    end

    if $db['artist'].length == 1 then    tweet << smartQuote( $db['artist'][0] )
    elsif $db['artist'].length == 2 then tweet << smartQuote( %Q{#{$db['artist'][0]}＆#{$db['artist'][1]}} )
    elsif $db['artist'].length >  2 then tweet << smartQuote( %Q{#{$db['artist'][0]} 𝑒𝑡 𝑎𝑙．} )
    end

    tweet << note(false)

    if $db['date'] != '' then tweet << "（#{$db['date']}）"
    else tweet << " "
    end

    #tweet << DateTime.now

    return tweet.join("")
end

##
# return the smart quoted string
def smartQuote(α)
    α.gsub( %r{([^\s，。．・／）〙〕』」])"}, '\1”' ).gsub( %r{"([^\s，。．・／])}, '“\1' )
end

if File.split($0).last == "nowplaying.rb" then
    if $*.length == 0 then          puts gen_output("track")
    elsif $*.index("track") then    puts gen_output("track")
    elsif $*.index("album") then    puts gen_output("album")
    else                            puts gen_output("track")
    end
end
